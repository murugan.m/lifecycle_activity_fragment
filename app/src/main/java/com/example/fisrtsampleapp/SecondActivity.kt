package com.example.fisrtsampleapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        this.title = "Second Activity"
        val  bundle:Bundle? = intent.extras
        val  idvalue:Int = (bundle?.get("id_value") ?: 10) as Int
        val  idname:String = (bundle?.get("id_name") ?: "english") as String

        val textView = findViewById<TextView>(R.id.textView)
        textView.text = "Name is $idname and Values is $idvalue"

        val backButton = findViewById<Button>(R.id.backButton)
        backButton.setOnClickListener {

            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
//            this.finish()
        }
    }

    override fun onStart() {
        super.onStart()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onContentChanged() {
        super.onContentChanged()
        println("SecondActivity  Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onStop() {
        super.onStop()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onPause() {
        super.onPause()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onResume() {
        super.onResume()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onRestart() {
        super.onRestart()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onPostResume() {
        super.onPostResume()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        println("SecondActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }
}